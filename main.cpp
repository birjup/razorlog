#include <iostream>
#include <future>
#include <utility>
#include <deque>
#include <thread>
#include <experimental/optional>
/**
 * @copyright Birju Prajapati
 *
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * WORK IN PROGRESS
 *
 * massive throughput ultra low latency logger
 * achieved via copying log args into tuple and not doing any i/o on the calling thread
 * storing in deferred future
 * pushing to thread_local simple wait-free spsc queue
 * one thread pops all queues and does the printf, expands the tuple into a parameter pack
 *


 */
namespace batlog {

    using std::shared_ptr;
    using std::atomic;
    using std::memory_order_acquire;
    using std::memory_order_release;
    using std::memory_order_relaxed;
    using std::future;
    using std::experimental::optional;

    template<typename T, size_t Size>
    class spsc_wait_free_queue {
    public:
        spsc_wait_free_queue() : head_(0), tail_(0) {}

        void push_spin (T && value) {
            while (!push(move(value)));
        }

        bool push( T && value)
        {
            size_t head = head_.load(memory_order_relaxed);
            size_t next_head = next(head);
            std::cout << next_head << std::endl;
            if (next_head == tail_.load(memory_order_acquire)) {
                return false;
            }
            ring_[head] = std::move(value);
            head_.store(next_head, memory_order_release);
            return true;
        }
        optional<T> pop()
        {
            size_t tail = tail_.load(memory_order_relaxed);
            if (tail == head_.load(memory_order_acquire)) {
                return {};
            }
            tail_.store(next(tail), memory_order_release);
            return move(ring_[tail_]);

        }
    private:
        size_t next(size_t current)
        {
            return (current + 1) % Size;
        }
        T ring_[Size];
        atomic<size_t> head_, tail_;
    };

    thread_local spsc_wait_free_queue<future<void>,1024> q;

    enum class level : char {
        trace = 'T',
        debug = 'D',
        info = 'I',
        warn = 'W',
        error = 'E',
        critical = 'C'
    };

    using namespace std;

    template <typename Tuple, size_t... Is>
    void tuple_printf(const char* pattern,  const Tuple& t, index_sequence<Is...>){
        printf(pattern, get<Is>(t)...);
    }

    template<typename... Ts>
    void log (level l, const char* pattern, Ts... ts) {

        //do we really need packaged_task noise if we can just defer?
        auto fut = std::async(std::launch::deferred,
                [str = pattern, argTuple = make_tuple(move(ts)...)](){
                    tuple_printf(str, argTuple, std::index_sequence_for<Ts...>{});
                });

        q.push(std::move(fut));

    }

}

int main()
{

    int howmany = 1000000;
         int numthreads = 1;

    namespace bl = batlog;


    std::atomic<int > msg_counter {0};
    std::vector<std::thread> threads;



    for (int t = 0; t < numthreads; ++t)
    {
        threads.push_back(std::thread([&]()
                                      {
                                          while (true)
                                          {
                                              int counter = ++msg_counter;
                                              if (counter > howmany) break;
                                              //std::cout << counter << std::endl;
                                              batlog::log(bl::level::info, "razorlog message %d: This is some text for your pleasure\n", counter);
                                          }
                                      }));
    }

    msg_counter = 0;
    auto t = std::thread([&](){
        while (true)
        {
            int counter = ++msg_counter;
            if (counter > howmany) break;
            //std::cout << counter << std::endl;
            auto f = std::move(bl::q.pop());
            if (f) {
                f->get();
            }
        }
    });

    for(auto &t:threads)
    {
        t.join();
    };


    //std::cout<< bl::q.size_approx() << std::endl;
//t.join();
    return 0;
}

